<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
		$role_admin = Role::where('name', 'Role_admin')->first();
		$role_user  = Role::where('name', 'Role_user')->first();
		
		$admin = new User();
		$admin->name = 'Administrator';
		$admin->email = 'admin@example.com';
		$admin->password = bcrypt('12345678');
		$admin->save();
		$admin->roles()->attach($role_admin);
		
		$user = new User();
		$user->name = 'User';
		$user->email = 'user@example.com';
		$user->password = bcrypt('12345678');
		$user->save();
		$user->roles()->attach($role_user);
	}
}
