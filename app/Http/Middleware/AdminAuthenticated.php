<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AdminAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		if(Auth::check() && Auth::user()->roles == 'Role_admin'){
			return $next($request);
		}
			dd(Auth::user()->roles);
		//return redirect()->route('error404');
		
		die();
    }
}
